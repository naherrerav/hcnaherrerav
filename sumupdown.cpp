#include <iostream>
#include <cstdlib>

typedef float REAL; //cuando envío a REAL será un double!!

REAL sumup(const int N);
REAL sumdown(const int N);

int main(int argc,char **argv)
{
	//falta verificar el número de argumentos, que atoi no retorne error, etc.
    const int N = atoi(argv[1]);
        std::cout.setf(std::ios::scientific);
        for(int i=0;i<N;i++){
		std::cout << i<< " " <<sumup(i) << " " << sumdown(i) <<std::endl;  
	}
return 0;
}

REAL sumup(const int N){
	REAL sum=0.0;
	for(int ii=1; ii<=N ; ++ii){
		sum+=1.0/ii;
	}
	return sum;
}

REAL sumdown(const int N){
	REAL sum=0.0;
	for(int ii=N; ii>=1 ; ii--){
		sum+=1.0/ii;
	}
	return sum;
	
}

#include <iostream>
#include <stdlib.h>
#include <cstdlib>
#include <math.h>
#include <time.h>
#include <string>
#include <utility>
#include <map>

using namespace std;

int main()
{
    int j,k, N=20;
    double a,p;
    srand(time(NULL));
    map<int, int> s;
    
     s[1]=N;
     cout<<"s "<<1<<"     "<<s[1]<<endl;
    for(int i=2;i<=N;i++){ // Creación distribución inicial
        s[i]=0;
        cout<<"s"<<i<<"     "<<s[i]<<endl;
    } 
    
    
    for(int i=1;i<=5;i++){ //Ciclo de iteraciones con elección a partir de v
        a=drand48();
        cout<<".-Aleatorio = "<<a<<endl;
        if(a<0.01){ //Fragmentación
            j=1+rand()%N;
            if(s[j]>0){				
            s[j]=s[j]-1;
            s[1]=s[1]+1;
          cout<<i<<" "<<j<<" "<<s[j]<<" "<<s[1]<<endl;
	  }
        }
        
        if(a>=0.01){ //Coalición
            j=1+rand()%N;
            k=1+rand()%N;
            if(s[j]>0 && s[k]>0 &&j+k<=N){
            s[j]=s[j]-1;
            s[k]=s[k]-1;
            s[j+k]=s[j+k]+1;
            cout<<i<<" "<<j<<" "<<s[j]<<" "<<s[k]<<" "<<s[j+k]<<endl;
		}
        }
    }
    
    for(int i=1;i<=N;i++){
        cout<<"s_"<<i<<"     "<<s[i]<<endl;
    }
    
    
    return 0;
}



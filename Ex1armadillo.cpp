//Programa que multiplica dos matrices aleatorias
//Compilar con g++ Ex1armadillo.cpp -o Ex1 -O1 -larmadillo
#include <iostream>
#include <armadillo>
#include <cstdlib>

using namespace std;
using namespace arma;

int main(int argc,char** argv)
{
 srand(10);
 const int N=atoi(argv[1]);
 mat A,B,C;
 
 for(int ii=0;ii<N;ii++){
 //initialization
 A=randu<mat>(4,5);
 B=randu<mat>(4,5);
 
 //main processing
 C=A*B.t();
}

 //cout<<C<<endl;  
	return 0;
}


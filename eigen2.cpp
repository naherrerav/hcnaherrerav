//Programa que opera una matriz aleatoria con un vector

#include <iostream>
#include <eigen3/Eigen/Dense> //#include<eigen3/Eigen/Dense> Se utilizaría esto para compilar sin una bandera especial para que encuentre la librería

using namespace Eigen;
using namespace std;

int main()
{
 srand48(0); //Es importante mantener la semilla CONSTANTE
 MatrixXd m=MatrixXd::Random(3,3);
 m=(m + MatrixXd::Constant(3,3,1.2))*50;
 cout << "m= " << endl << m << endl;
 VectorXd v(3);
 v << 1,2,3;
 cout <<"m*v= "<<endl<<m*v<<endl;
 return 0;	
}

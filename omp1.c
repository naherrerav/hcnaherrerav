//ejecutar con gcc omp1.c -fopenmp
// para decirle el número de threads export OMP_NUM_THREADS=8
#include<stdio.h>

int main(void)
{
  #pragma omp parallel
  {
  printf("Hello, World.\n");
  printf("Yo soy el thread : %d\n", omp_get_thread_num());
  }
  return 0;
 }

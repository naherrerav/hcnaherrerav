//function example for max value between two values using if-else, operador ternario and plantilla

#include<iostream>

//int getMaxInt(int ii,int jj);
//int getMax(int ii,int jj);
//int getMax(double ii,double jj);
template <class T,class S> T getMax(T a,S b);

int main(void)
{
	int m =5, n=10;
	double x=6.9, y=0.9;
	std::cout <<getMax(m,n) <<std::endl;
	std::cout <<getMax(x,y) <<std::endl;	
	std::cout <<getMax(x,n) <<std::endl;
	
	return 0;
}

//int getMaxInt(int ii,int jj) Forma if-else
//{
//	if(ii>jj)
//	 return ii;
//	else return jj;
//}

//int getMax(int ii,int jj)
//{
//	int max = (ii > jj) ? ii : jj; //operador ternario!!! Si () es verdad selecciona el primero, si no, selecciona el segundo. Reemplaza if-else
//	return max;
//}

//int getMax(double ii,double jj)
//{
//	double max = (ii > jj) ? ii : jj; 
//	return max;
//}

template <class T,class S> T getMax(T a,S b)  //función plantilla que permite generalizar la forma de una función. La clase de las variables la dan T y S.
{
	T max = (a > b) ? a : b; //El valor de retorno será tipo T
	return max;
}



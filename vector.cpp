#include <iostream>
#include <vector>
#include <algorithm>

int main(int argc, char **argv)
{
	std::vector<double> data;
	int size = 0;
	
	//std::cout <<"Size of the array? -> " << std::endl; //endl es similar al \n. 
	std::clog << "Size of the array? -> " << std::endl;
	//Si se quiere redireccionar la salida estándar, puedo enviar a a consola con clog o cerr y ejecutando ./vector.out 2> vector.txt
	std::cin >> size;
	
	data.resize(size);
	
	for(int ii = 0; ii < size; ++ii){
		data[ii] = 2*ii;
	}
	
	for(int ii = 0; ii < size; ++ii){ 
		std::cout << ii << "\t" << data[ii] << std::endl;	
	}

    return 0;
}



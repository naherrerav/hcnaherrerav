// requires c++11
// create the Particle class
// uses vectors
// Add force function (only gravitational)
// Loop to compute parabolic movement, integration with euler
// Implements leapfrog
// Adds gnuplot ploting function. Data is printed to file
// Added bouncing with the ground
// Extended for several particles
// Added more particles amnd force among them

#include <cstdlib>
#include <cstdio>
#include <vector>
#include "vector.h"

//----------------------------------------------------------
// constants
//----------------------------------------------------------
const int NX = 2; 
const int NZ = 1;
const int N = NX*NZ;
const double G = 9.8; // m/s^2
const double DT = 1.0e-2; // s
const double NITER = 800; // total time steps
const double MAX_RAD = 0.1;
const double LX = NX*2*MAX_RAD;
const double LZ = NZ*2*MAX_RAD;
const double K = 1000.2;

//----------------------------------------------------------
// Class Particle : Definition and implementation
//----------------------------------------------------------
class Particle {
public: // data
  vector3D R_, V_, F_; 
  double mass_ = 0;
  double rad_ = 0;
  
public : // methods
  Particle(const vector3D & R, const vector3D & V, const vector3D & F, 
	   const double & mass, const double & rad);
  Particle() {};

  void print_to_gnuplot(void);
};


Particle::Particle(const vector3D & R, const vector3D & V, const vector3D & F, 
		   const double & mass, const double & rad)
{
  R_ = R; V_ = V; F_ = F; 
  mass_ = mass; 
  rad_ = rad;
}

void Particle::print_to_gnuplot(void)
{
  std::cout << R_.x() <<  " + " << rad_ << "*cos(2*PI*t) , " 
	    << R_.z() <<  " + " << rad_ << "*sin(2*PI*t) ";  
}


//----------------------------------------------------------
// Function declaration
//----------------------------------------------------------
void compute_forces(std::vector<Particle> & body);

void start_time_integration(std::vector<Particle> & body);
void time_step(std::vector<Particle> & body);

void set_particle_properties(std::vector<Particle> & body);
void set_initial_conditions(std::vector<Particle> & body);

void configure_gnuplot(void);
void start_gnuplot_command(void);
void end_gnuplot_command(void);

//----------------------------------------------------------
// Main function
//----------------------------------------------------------
int main(int argc, char **argv)
{
  std::vector<Particle> balls; balls.resize(N);
  
  // Properties
  set_particle_properties(balls);

  // Initial Conditions
  set_initial_conditions(balls);
  start_time_integration(balls);

  // data file
  FILE *fout; fout = std::fopen("data.dat", "w");

  // gnuplot
  configure_gnuplot();

  // loop over time
  for (int iter = 0; iter < NITER; ++iter) {
    // printing
    double time = 0.0 + iter*DT;
    std::fprintf(fout, "%23.16e\t%23.16e\t%23.16e\t%23.16e\n", time, balls[0].R_.x(), balls[0].R_.y(), balls[0].R_.z());
    
    //gnuplot
    start_gnuplot_command();
    for (int ii = 0; ii < N; ++ii) {
      balls[ii].print_to_gnuplot();
      if (ii < N - 1 ) std::cout << " , "; 
    }      
    end_gnuplot_command();

    // system advancing in time
    time_step(balls);
    compute_forces(balls);
  }

  std::fclose(fout);

  return 0;   
}

//----------------------------------------------------------
// Function implementation 
//----------------------------------------------------------
void compute_forces(std::vector<Particle> & body)
{
  vector3D tmp;
  
  // reset force
  for (auto & b : body) { 
    b.F_.cargue(0, 0, 0);
  }
  
  // add gravity force
  tmp.cargue(0, 0, -G);
  for (auto & b : body) { 
    b.F_ += b.mass_*tmp;
  }

  // forces with planes
  double delta = 0;
  // bottom plane
  for (auto & b : body) { 
    delta = b.R_.z() - b.rad_;
    if (delta < 0) {
      tmp.cargue(0, 0, -delta);
      b.F_ += K*tmp;
    }
  }
}

void set_particle_properties(std::vector<Particle> & body)
{
  for (auto & b : body) {
    b.mass_ = 1.0;
    b.rad_ = MAX_RAD;
  }
}

void set_initial_conditions(std::vector<Particle> & body)
{
  // ix = idx%Nx, iy = idx/Nx
  for (int idx = 0 ; idx < N; ++idx) {
    body[idx].R_.cargue(MAX_RAD + 2.1*MAX_RAD*(idx%NX), 0.0, MAX_RAD + 2.1*MAX_RAD*(idx/NX));
    body[idx].V_.cargue(0, 0, 1.0);
    body[idx].F_.cargue(0, 0, 0.0);
  }
}

void start_time_integration(std::vector<Particle> & body)
{
  // LeapFrog
  for (auto & b : body) {
    b.V_ -= 0.5*DT*b.F_/b.mass_;
  }
}

void time_step(std::vector<Particle> & body)
{
  // Euler
  // for (auto & b : body) {
  //b.R_ += b.V_*DT + 0.5*b.F_*DT*DT/b.mass_;
  //b.V_ += b.F_*DT/b.mass_;
  
  // LeapFrog
  for (auto & b : body) {
    b.V_ += DT*b.F_/b.mass_;
    b.R_ += DT*b.V_;
  }
  
}

void configure_gnuplot(void)
{
  std::cout << "set parametric" << std::endl;
  std::cout << "set trange [0:1]" << std::endl;
  std::cout << "set xrange [-1:2]" << std::endl;
  std::cout << "set yrange [-1:3]" << std::endl;
  std::cout << "unset key" << std::endl;
  std::cout << "set size ratio -1 " << std::endl;
  std::cout << "PI=3.1415926" << std::endl;
}

void start_gnuplot_command(void)
{
  std::cout << "plot " ;
}

void end_gnuplot_command(void)
{
  std::cout << std::endl;
}


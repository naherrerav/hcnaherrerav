// requires c++11
// create the Particle class
// uses vectors
// Add force function (only gravitational)
// Loop to compute parabolic movement, integration with euler
// Implements leapfrog
// Adds gnuplot ploting function. Data is printed to file

#include <cstdlib>
#include <cstdio>
#include <fstream>
#include "vector.h"

//----------------------------------------------------------
// constants
//----------------------------------------------------------
const double G = 9.8; // m/s^2
const double DT = 1.0e-2; // s
const double NITER = 200; // total time steps

//----------------------------------------------------------
// Class Particle : Definition and implementation
//----------------------------------------------------------
class Particle {
public: // data
  vector3D R_, V_, F_; 
  double mass_ = 0;
  double rad_ = 0;
  
public : // methods
  Particle(const vector3D & R, const vector3D & V, const vector3D & F, 
	   const double & mass, const double & rad);
  Particle() {};

  void print_to_gnuplot(void);
};


Particle::Particle(const vector3D & R, const vector3D & V, const vector3D & F, 
		   const double & mass, const double & rad)
{
  R_ = R; V_ = V; F_ = F; 
  mass_ = mass; 
  rad_ = rad;
}

void Particle::print_to_gnuplot(void)
{
  std::cout << R_.x() <<  " + " << rad_ << "*cos(2*PI*t) , " 
	    << R_.z() <<  " + " << rad_ << "*sin(2*PI*t) ";  
}


//----------------------------------------------------------
// Function declaration
//----------------------------------------------------------
void compute_forces(Particle & body);

void start_time_integration(Particle & body);
void time_step(Particle & body);

void set_particle_properties(Particle & body);
void set_initial_conditions(Particle & body);

void configure_gnuplot(void);
void start_gnuplot_command(void);
void end_gnuplot_command(void);

//----------------------------------------------------------
// Main function
//----------------------------------------------------------
int main(int argc, char **argv)
{
  Particle ball;
  
  // Properties
  set_particle_properties(ball);

  // Initial Conditions
  set_initial_conditions(ball);
  start_time_integration(ball);

  // data file
  FILE *fout; fout = std::fopen("data.dat", "w");

  // gnuplot
  configure_gnuplot();

  // loop over time
  for (int iter = 0; iter < NITER; ++iter) {
    // printing
    double time = 0.0 + iter*DT;
    std::fprintf(fout, "%23.16e\t%23.16e\t%23.16e\t%23.16e\n", time, ball.R_.x(), ball.R_.y(), ball.R_.z());
    
    //gnuplot
    start_gnuplot_command();
    ball.print_to_gnuplot();
    end_gnuplot_command();

    // system advancing in time
    time_step(ball);
    compute_forces(ball);
  }

  std::fclose(fout);

  return 0;   
}

//----------------------------------------------------------
// Function implementation 
//----------------------------------------------------------
void compute_forces(Particle & body)
{
  vector3D tmp;
  
  // reset force
  body.F_.cargue(0, 0, 0);
  
  // add gravity force
  tmp.cargue(0, 0, -G);
  body.F_ += body.mass_*tmp;
}

void set_particle_properties(Particle & body)
{
  body.mass_ = 1.0;
  body.rad_ = 0.1;
}

void set_initial_conditions(Particle & body)
{
  body.R_.cargue(2, 0, 20.0);
  body.V_.cargue(0, 0, 1.0);
  body.F_.cargue(0, 0, 0.0);
}

void start_time_integration(Particle & body)
{
  // LeapFrog
  body.V_ -= 0.5*DT*body.F_/body.mass_;
}

void time_step(Particle & body)
{
  // Euler
  //body.R_ += body.V_*DT + 0.5*body.F_*DT*DT/body.mass_;
  //body.V_ += body.F_*DT/body.mass_;
  
  // LeapFrog
  body.V_ += DT*body.F_/body.mass_;
  body.R_ += DT*body.V_;
  
}

void configure_gnuplot(void)
{
  std::cout << "set parametric" << std::endl;
  std::cout << "set trange [0:1]" << std::endl;
  std::cout << "set xrange [0:4]" << std::endl;
  std::cout << "set yrange [1:21]" << std::endl;
  std::cout << "unset key" << std::endl;
  std::cout << "set size ratio -1 " << std::endl;
  std::cout << "PI=3.1415926" << std::endl;
}

void start_gnuplot_command(void)
{
  std::cout << "plot " ;
}

void end_gnuplot_command(void)
{
  std::cout << std::endl;
}


// needs -std=c++11 for compilation
// implements class Particle
// Adds vectors
// Added gravity force

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include "vector.h"

//--------------------------------------------
// Constants
//--------------------------------------------
const int NX = 6;
const int NZ = 6;
const int N = NX*NZ;
const double RAD_MAX = 0.56; // m
const double LX = NX*2*RAD_MAX; // m
const double LZ = NZ*2*RAD_MAX; // m
const double G = 0.0; // m/s**2
const double K = 2023.99; // Kg/s**2

//--------------------------------------------
// Class Particle
//--------------------------------------------
class Particle {
public:
 double mass_, rad_;
 vector3D R_, V_, F_;
 
public:
 Particle();
 Particle(double mass0, double rad0);

 double x() { return R_.x(); }
 double y() { return R_.y(); }
 double z() { return R_.z(); }

 void print_to_gnuplot(void);
};

Particle::Particle()
{
 mass_ = rad_ = 0;
 R_.cargue(0, 0, 0);
 V_.cargue(0, 0, 0);
 F_.cargue(0, 0, 0);

}

Particle::Particle(double mass0, double rad0)
{
 mass_ = mass0;
 rad_ = rad0;
 R_.cargue(0, 0, 0);
 V_.cargue(0, 0, 0);
 F_.cargue(0, 0, 0);
}

void Particle::print_to_gnuplot(void)
{
 std::cout << R_.x() << " + " << rad_ << "*cos(2.0*PI*t)"
   << " , "
   << R_.z() << " + " << rad_ << "*sin(2.0*PI*t)";
}


//--------------------------------------------
// Function declarations
//--------------------------------------------
void compute_forces(std::vector<Particle> & bodies);
void time_step(std::vector<Particle> & bodies, const double & dt);
void start_time_integration(std::vector<Particle> & bodies, const double & dt);
void configure_gnuplot(void);
void start_gp_command(void);
void end_gp_command(void);
void print_walls_to_gnuplot(void);
void preprocessing(std::vector<Particle> & bodies);

//--------------------------------------------
// Main function
//--------------------------------------------
int main(int argc, char **argv)
{
 // constants
 const double DELTAT = 1.0e-2; // secs
 const int NITER = 2000;

 // pre-processing
 std::vector<Particle> balls(N);
 preprocessing(balls);

 // time integration start
 compute_forces(balls);
 start_time_integration(balls, DELTAT);

 // Printing streams
 FILE *pf;
 pf = std::fopen("data.dat", "w");


 // processing
 configure_gnuplot();
 for(int iter = 0; iter < NITER; iter++) {
   double time = 0.0 + iter*DELTAT;

   // gnuplot
   start_gp_command();
   for (int idx = 0; idx < balls.size(); ++idx) {
     balls[idx].print_to_gnuplot();
     std::cout << ",";
   }
   print_walls_to_gnuplot();
   end_gp_command();

   // printing
   std::fprintf(pf, "%23.16e\t%23.16e\n", time, balls[0].R_.z());

   // time step
   compute_forces(balls);
   time_step(balls, DELTAT);
 }

 std::fclose(pf);

 return 0;
}

//--------------------------------------------
// Function definitions
//--------------------------------------------
void compute_forces(std::vector<Particle> & bodies)
{
 // reset forces
 for (int idx = 0; idx < bodies.size(); ++idx) {
   bodies[idx].F_.cargue(0, 0, 0);
 }

 // gravity
 vector3D tmp;
 tmp.cargue(0, 0, -G); // vector g
 for (int idx = 0; idx < bodies.size(); ++idx) {
   bodies[idx].F_ += bodies[idx].mass_*tmp;
 }

 // forces against walls
 for (int idx = 0; idx < bodies.size(); ++idx) {
   // force against bottom wall
   double h = bodies[idx].rad_ - bodies[idx].R_.z();
   if (h > 0) {
     tmp.cargue(0, 0, K*h);
     bodies[idx].F_ += tmp;
   }
   // force against top wall
   h = bodies[idx].rad_ + bodies[idx].R_.z() - LZ;
   if (h > 0) {
     tmp.cargue(0, 0, -K*h);
     bodies[idx].F_ += tmp;
   }
   // force against left wall
   h = bodies[idx].rad_ - bodies[idx].R_.x();
   if (h > 0) {
     tmp.cargue(K*h, 0, 0);
     bodies[idx].F_ += tmp;
   }
   // force against right wall
   h = bodies[idx].rad_ + bodies[idx].R_.x() - LX;
   if (h > 0) {
     tmp.cargue(-K*h, 0, 0);
     bodies[idx].F_ += tmp;
   }
 }

 // forces among particles
 for (int idx = 0; idx < bodies.size(); ++idx) {
   for (int jdx = idx+1; jdx < bodies.size(); ++jdx) {
     vector3D Rij = bodies[idx].R_ - bodies[jdx].R_;
     double h = bodies[idx].rad_ + bodies[jdx].rad_ - norma(Rij);
     if (h > 0) {
bodies[idx].F_ += K*h*Rij/norma(Rij);
bodies[jdx].F_ -= K*h*Rij/norma(Rij);
     }
   }
 }

}

void time_step(std::vector<Particle> & bodies, const double & dt)
{
 // Euler
 //body.R_ = body.R_ + dt*body.V_ + 0.5*dt*dt*body.F_/body.mass_;
 //body.V_ = body.V_ + dt*body.F_/body.mass_;

 // Leapfrog
 for (int idx = 0; idx < bodies.size(); ++idx) {
   bodies[idx].V_ += dt*bodies[idx].F_/bodies[idx].mass_;
   bodies[idx].R_ += dt*bodies[idx].V_;
 }
}

void start_time_integration(std::vector<Particle> & bodies, const double & dt)
{
 // Leapfrog
 for (int idx = 0; idx < bodies.size(); ++idx) {
   bodies[idx].V_ -= (bodies[idx].F_/bodies[idx].mass_)*dt*dt*0.5;
 }
}

void configure_gnuplot(void)
{
 std::cout << "set parametric" << std::endl;
 std::cout << "unset key" << std::endl;
 std::cout << "set trange [0:1]" << std::endl;
 std::cout << "PI=3.1415926" << std::endl;
 std::cout << "set size ratio -1" << std::endl;
 std::cout << "set xrange [-2:" << 1.2*LX << "]" << std::endl;
 std::cout << "set yrange [-2:" << 1.2*LZ << "]" << std::endl;
}

void start_gp_command(void)
{
 std::cout << "plot ";
}

void end_gp_command(void)
{
 std::cout << std::endl;
}

void print_walls_to_gnuplot(void)
{
 // bottom wall
 std::cout << "t*" << LX << ", 0 lt -1";
 // top wall
 std::cout << " , ";
 std::cout << "t*" << LX << ", " << LZ << " lt -1";
 // left wall
 std::cout << " , ";
 std::cout << 0 << ", t*" << LZ << " lt -1";
 // right wall
 std::cout << " , ";
 std::cout << LX << ", t*" << LZ << " lt -1";
}

void preprocessing(std::vector<Particle> & bodies)
{
 for(int idx = 0; idx < bodies.size(); ++idx) {
   const double rmin = 0.50*RAD_MAX;
   const double rmax = 0.95*RAD_MAX;
   bodies[idx].rad_ = rmin + drand48()*(rmax-rmin);
   bodies[idx].mass_ = 1.2;
   // initial positions
   double x = ((idx%NX)*2 + 1)*RAD_MAX;
   double z = ((idx/NX)*2 + 1)*RAD_MAX;
   bodies[idx].R_.cargue(x, 0, z);
   // random velocity
   const double V0 = 1.23;
   const double angle = drand48()*2*M_PI;
   bodies[idx].V_.cargue(V0*std::cos(angle), 0, V0*std::sin(angle));
 }

}

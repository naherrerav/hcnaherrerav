// requires c++11
// Creates the Particle class, using scalars

#include <iostream>

class Particle {
private:
  double Rx_ = 0, Ry_ = 0, Rz_ = 0;
  double Vx_ = 0, Vy_ = 0, Vz_ = 0;
  double Fx_ = 0, Fy_ = 0, Fz_ = 0;
  double mass_ = 0;
  double rad_ = 0;

public :
  Particle(double & Rx, double & Ry, double & Rz, 
	   double & Vx, double & Vy, double & Vz, 
	   double & Fx, double & Fy, double & Fz, 
	   double & mass, double & rad);
  Particle() {};

};


Particle::Particle(double & Rx, double & Ry, double & Rz, 
		   double & Vx, double & Vy, double & Vz, 
		   double & Fx, double & Fy, double & Fz, 
		   double & mass, double & rad)
{
  Rx_ = Rx; Ry_ = Ry; Rz_ = Rz;
  Vx_ = Vx; Vy_ = Vy; Vz_ = Vz;
  Fx_ = Fx; Fy_ = Fy; Fz_ = Fz;
  mass_ = mass; 
  rad_ = rad;
}


int main(int argc, char **argv)
{
  Particle ball;
 
  return 0;   
}

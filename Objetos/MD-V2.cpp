// requires c++11
// create the Particle class
// uses vectors
// Add force function (only gravitational)

#include <iostream>
#include "vector.h"

//----------------------------------------------------------
// constants
//----------------------------------------------------------
const double G = 9.8; // m/s^2

//----------------------------------------------------------
// Class Particle : Definition and implementation
//----------------------------------------------------------
class Particle {
public: // data
  vector3D R_, V_, F_; 
  double mass_ = 0;
  double rad_ = 0;
  
public : // methods
  Particle(const vector3D & R, const vector3D & V, const vector3D & F, 
	   const double & mass, const double & rad);
  Particle() {};
};


Particle::Particle(const vector3D & R, const vector3D & V, const vector3D & F, 
		   const double & mass, const double & rad)
{
  R_ = R; V_ = V; F_ = F; 
  mass_ = mass; 
  rad_ = rad;
}

//----------------------------------------------------------
// Function declaration
//----------------------------------------------------------
void compute_forces(Particle & body);


//----------------------------------------------------------
// Main function
//----------------------------------------------------------
int main(int argc, char **argv)
{
  Particle ball;
  ball.mass_ = 1.0;

  ball.R_.show(); 
  ball.F_.show(); 
  compute_forces(ball);
  ball.R_.show(); 
  ball.F_.show(); 

  return 0;   
}

//----------------------------------------------------------
// Function implementation 
//----------------------------------------------------------
void compute_forces(Particle & body)
{
  vector3D tmp;
  
  // reset force
  body.F_.cargue(0, 0, 0);
  
  // add gravity force
  tmp.cargue(0, 0, -G);
  body.F_ += body.mass_*tmp;
}

// requires c++11
// Creates the particle class
// uses vectors

#include <iostream>
#include "vector.h"

class Particle {
private:
  vector3D R_, V_, F_; 
  double mass_ = 0;
  double rad_ = 0;
  
public :
  Particle(const vector3D & R, const vector3D & V, const vector3D & F, 
	   const double & mass, const double & rad);
  Particle() {};

};


Particle::Particle(const vector3D & R, const vector3D & V, const vector3D & F, 
		   const double & mass, const double & rad)
{
  R_ = R; V_ = V; F_ = F; 
  mass_ = mass; 
  rad_ = rad;
}


int main(int argc, char **argv)
{
  Particle ball;
 
  return 0;   
}

// requires c++11
// create the Particle class
// uses vectors
// Add force function (only gravitational)
// Loop to compute parabolic movement, integration with euler

#include <iostream>
#include "vector.h"

//----------------------------------------------------------
// constants
//----------------------------------------------------------
const double G = 9.8; // m/s^2
const double DT = 1.0e-2; // s
const double NITER = 1000; // total time steps

//----------------------------------------------------------
// Class Particle : Definition and implementation
//----------------------------------------------------------
class Particle {
public: // data
  vector3D R_, V_, F_; 
  double mass_ = 0;
  double rad_ = 0;
  
public : // methods
  Particle(const vector3D & R, const vector3D & V, const vector3D & F, 
	   const double & mass, const double & rad);
  Particle() {};
};


Particle::Particle(const vector3D & R, const vector3D & V, const vector3D & F, 
		   const double & mass, const double & rad)
{
  R_ = R; V_ = V; F_ = F; 
  mass_ = mass; 
  rad_ = rad;
}

//----------------------------------------------------------
// Function declaration
//----------------------------------------------------------
void compute_forces(Particle & body);
void time_step(Particle & body);
void set_particle_properties(Particle & body);
void set_initial_conditions(Particle & body);

//----------------------------------------------------------
// Main function
//----------------------------------------------------------
int main(int argc, char **argv)
{
  Particle ball;
  
  // Properties
  set_particle_properties(ball);

  // Initial Conditions
  set_initial_conditions(ball);

  // loop over time
  for (int iter = 0; iter < NITER; ++iter) {
    // printing
    double time = 0.0 + iter*DT;
    std::printf("%23.16e\t%23.16e\t%23.16e\t%23.16e\n", time, ball.R_.x(), ball.R_.y(), ball.R_.z());

    // system advancing in time
    time_step(ball);
    compute_forces(ball);
  }
  return 0;   
}

//----------------------------------------------------------
// Function implementation 
//----------------------------------------------------------
void compute_forces(Particle & body)
{
  vector3D tmp;
  
  // reset force
  body.F_.cargue(0, 0, 0);
  
  // add gravity force
  tmp.cargue(0, 0, -G);
  body.F_ += body.mass_*tmp;
}

void time_step(Particle & body)
{
  body.R_ += body.V_*DT + 0.5*body.F_*DT*DT/body.mass_;
  body.V_ += body.F_*DT/body.mass_;
}

void set_particle_properties(Particle & body)
{
  body.mass_ = 1.0;
  body.rad_ = 0.1;
}

void set_initial_conditions(Particle & body)
{
  body.R_.cargue(2, 0, 20.0);
  body.V_.cargue(0, 0, 1.0);
  body.F_.cargue(0, 0, 0.0);
}

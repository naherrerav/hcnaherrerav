#include <iostream>
#include <math.h>
#include <cstdlib>

using namespace std;

typedef float REAL;
typedef REAL (*F)(REAL);

REAL integrate(const REAL &a, const REAL &b, const int N);

int main(int argc, char **argv)
{
  //to check: b>a, argc > 0, etc
  const REAL a =atof(argv[1]);
  const REAL b =atof(argv[2]);
  const int NMAX =atof(argv[3]);
  
  cout.precision(16);
  //std::cout.setf(std::ios::scientific);
  for(int ii =1; ii<=NMAX; ++ii){
	 cout <<ii << " " << integrate(a, b, ii) <<endl; 
  }

 return 0;	
}

REAL integrate(const REAL &a, const REAL &b, const int N){
	const REAL dx=(b-a)/N;
	REAL sum =(sin(a)+sin(b))/2.0;
	
	for(int ii=1;ii<=N-1;++ii){
		sum+=sin(a+ii*dx);
	}
	sum*=dx;
	return sum;
}

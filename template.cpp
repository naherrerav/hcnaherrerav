#include<iostream>
#include<vector>
#include<algorithm>

template<class T> void print(const T & vec);

int main(int argc, char **argv)
{
	std::vector<double> data;
	data.resize(21);
	std::fill(data.begin(), data.end(), 0.0);
	print(data);
	
	for(auto x : data) { x +=10; }
	print(data);
	
	for(auto &x : data) {x += 10;}
	print(data);
	
	return 0;
}


template<class T> void print(const T & vec)
{
	for (const auto &x : vec){
		std::cout << x << "\t";
	}
	std::cout <<std::endl;
}

#include <stdio.h>
#include <mpi.h> //gcc omp2.c -I /usr/lib/openmpi/include/ -lmpi
//run with mpirun -np 4 ./a.out ----> np is the number of processes and mpirun is the one that creates the new ...
//better option for compilation: mpicc omp2.c

int main(int argc, char **argv)
{
 int rank,size;
 MPI_Init(&argc,&argv);
 MPI_Comm_size(MPI_COMM_WORLD,&size);
 MPI_Comm_rank(MPI_COMM_WORLD,&rank);
 printf("Hello From process %d of %d\n",rank,size);
 MPI_Finalize ();
 return 0;
}

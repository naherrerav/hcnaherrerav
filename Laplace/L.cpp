// Finite diferences: Laplace equation in 2D
/*
1)Definir limites 
  Definir matriz
  Imprima la matriz
2) Funcion para condiciones Iniciales
3) Funcion para condiciones de frontera
4) Implemente diferencias finitas
5) Relaje
 */
#include <iostream>
#include <vector>
using namespace std;

//---------------------------------------------
// Limits
//---------------------------------------------
const double DX = 0.1; //Grid separation in x
const double DY = DX; //Grid separation in y
const double XF = 1.2; //Box dimension in x
const double YF = 1.3; //Box dimension in y
const int NX = int(XF/DX) + 1; //Size x for our matrix
const int NY = int(YF/DY) + 1; //size y for our matrix
typedef vector < vector<double> > Matrix;

//---------------------------------------------
// function declarations
//---------------------------------------------
void SetMemory(Matrix & grid);
void Print(Matrix & grid);
void PrintGnuplot(Matrix &grid);
void SetInitialConditions(Matrix & grid);
void SetBoundaryConditions(Matrix & grid);
void RelaxStep(Matrix & grid);
void Relax(Matrix & grid, int Niter);

//---------------------------------------------
// main function
//---------------------------------------------
int main(void)
{
  Matrix malla;
  SetMemory(malla);
  SetInitialConditions(malla);
  SetBoundaryConditions(malla);
  RelaxStep(malla);
  Relax(malla, 100);
  PrintGnuplot(malla);
    
  return 0;
}

//---------------------------------------------
// function implementation
//---------------------------------------------
void SetMemory(Matrix & grid)
{
  int i;
  grid.resize(NX);
  for(i = 0; i < NX; i++) {
    grid[i].resize(NY);
  }
}


void Print(Matrix & grid)
{
  int i, j;
  for(i = 0; i < NX; i++) {
    for(j = 0; j < NY; j++) {
      cout << i*DX << "\t"
	   << j*DY << "\t"
	   << grid[i][j] << "\t"
	   << endl;
    }
    cout << endl;
  }
}

void PrintGnuplot(Matrix &grid)
{
  //cout << "set term gif animate" << endl;                                          
  //cout << "set out 'LaplaceAnim.gif'" << endl;                                     
  cout << "set pm3d" << endl; //For color surface
  cout << "splot '-' w l lt 5" << endl; //splot for surface plotting using standard exit of the terminal with lines type 5
  Print(grid); //Data is sent
  cout << "e" << endl; //ending exit data
  cout << "pause mouse" << endl; //to avoid closing GNUplot
}

void SetInitialConditions(Matrix & grid)
{
  const double V0 = 10.2324;
  for(int ii = 0; ii < NX; ii++) {
    for(int jj = 0; jj < NY; jj++) {
      grid[ii][jj] = V0;
    }
  }
}

void SetBoundaryConditions(Matrix & grid)
{
  int i, j;
  // bottom wall
  j = 0;
  for(i=0; i < NX; i++) {
    grid[i][j] = 0;
  }
  // top wall
  j = NY-1;
  for(i=0; i < NX; i++) {
    grid[i][j] = 0;
  }
  // l-wall
  i = 0;
  for(j=0; j < NY; j++) {
    grid[i][j] = 0;
  }
  // r-wall
  i = NX-1;
  for(j=0; j < NY; j++) {
    grid[i][j] = 0;
  }
  // first internal bar L1
  j = NX/4;
  for(i=NY/4; i < 3*NY/4; i++) {
    grid[i][j] = -100;
  }
  // second internal bar L2
  j = 3*NX/4;
  for(i=NY/4; i < 3*NY/4; i++) {
    grid[i][j] = 100;
  }
  // Third internal bar L3
  i = 7*NX/8;
  for(j=NY/3; j < 2*NY/3; j++) {
    grid[i][j] = 80;
  }
}

void RelaxStep(Matrix & grid)
{
	int ii,jj;
	for(ii = 1; ii < NX-2; ii++) {
    for(jj = 1; jj < NY-2; jj++) {
	  if (jj==NX/4 && (NY/4<=ii && ii<3*NY/4)) continue;
      if (jj==3*NX/4 && (NY/4<=ii && ii<3*NY/4)) continue;
      if (ii==7*NX/8 && (NY/3<=jj && jj<2*NY/3)) continue;
      grid[ii][jj] = 0.25*(grid[ii+1][jj] + grid[ii-1][jj] + grid[ii][jj+1] + grid[ii][jj-1]);
    }
  }
}

void Relax(Matrix & grid, int Niter)
{
  cout << "set term gif animate" << endl;                                          
  cout << "set out 'LaplaceAnim.gif'" << endl; 
  int count;
  for(count = 1; count <= Niter; count++) {
    RelaxStep(grid);
    PrintGnuplot(grid);
  }
}


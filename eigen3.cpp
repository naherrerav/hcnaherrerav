//Programa que resuelve el sistema Ax=b utilizando Householder
#include <iostream>
#include <eigen3/Eigen/Dense>

using namespace std;
using namespace Eigen;

int main()
{
  Matrix3d A;
  Vector3d b;
  A <<1,2,3, 4,5,6,  7,8,10;
  b <<3,3,4;
  
  cout << "Matriz A :\n" << A <<endl;
  cout << "Vector b es:\n" << b <<endl;
  
  Vector3d x = A.colPivHouseholderQr().solve(b);
  cout << "vector x:\n" << x << endl;
  cout <<"Error:\n" << (A*x-b).norm()/b.norm() << endl;
  
	return 0;
}

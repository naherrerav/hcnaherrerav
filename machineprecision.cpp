#include <iostream>
#include <cstdlib>

typedef float REAL; //cuando envío a REAL será un double!!

int main(int argc,char **argv)
{
	//falta verificar el número de argumentos, que atoi no retorne error, etc.
    const int N = atoi(argv[1]);
    
    REAL eps=1.0;
    
    for (int ii = 0; ii < N ; ++ii) {
		eps /= 2.0;
		REAL one= 1.0 + eps;
		std::cout <<ii << " " << one <<" "<< eps <<std::endl;  	
	}

return 0;
}

//Resultado: el cout sólo escribe 6 decimales y la machine precision: e-45 para el float

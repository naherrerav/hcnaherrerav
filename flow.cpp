#include <iostream>
#include <cstdlib>

typedef float REAL; //cuando envío a REAL será un double!!

int main(int argc,char **argv)
{
	//falta verificar el número de argumentos, que atoi no retorne error, etc.
    const int N = atoi(argv[1]);
    
    REAL under=1.0, over=1.0;
    
    for (int ii = 0; ii < N ; ++ii) {
		under /= 2.0;
		over *= 2.0;
		std::cout <<ii << " " << under <<" "<< over <<std::endl;  
		
	}

return 0;
}

//Resultados: overfloat: 10e38 underfloat 10e-45

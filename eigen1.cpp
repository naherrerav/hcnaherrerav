//Programa que escribe una matriz. Para compilar se necesita gcc nombre.cpp -I/usr/include/eigen3 que aclara la ruta de la librería que necesitamos

#include <iostream>
#include <Eigen/Dense> //#include<eigen3/Eigen/Dense> Se utilizaría esto para compilar sin una bandera especial para que encuentre la librería

using Eigen::MatrixXd;

int main(int argc, char **argv)
{
 MatrixXd m(2, 2); //MatrixXd es un tipo de dato matricial donde X dice que el tamaño es dinámico y d para variables double
 m(0, 0)=3;
 
 std::cout <<m <<std::endl;
 return 0;	
}

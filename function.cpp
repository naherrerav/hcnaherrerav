//function example for Constant references

#include<iostream>
using namespace std;
int addition (const int &a,int b)
{
	int r;
	r=a+b; 
	//a=r; ERROR
	return (r);
}

int main()
{
	int z, x, y;
	x=5;
	y=3;
	z=addition (x,y);
	cout <<"The result is " << z <<std::endl; 
	cout <<"The value of x is " << x;
	return 0;
}

//Programa que halla vectores propios
//Compilar con g++ Ex2armadillo.cpp -o Ex2 -O1 -larmadillo
#include <iostream>
#include <armadillo>

using namespace std;
using namespace arma;

int main(int argc,char** argv)
{
 //srand(10);
 mat A=randu<mat>(5,5);
 mat B=A.t()*A; //generates a symmetric matrix
 
 vec eigval;
 mat eigvec;
 
 eig_sym(eigval,eigvec,B);
 
 cout<<B<<endl;
 cout<<eigval<<endl;
 cout<<eigvec<<endl;
	return 0;
}
